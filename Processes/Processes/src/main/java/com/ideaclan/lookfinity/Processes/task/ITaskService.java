package com.ideaclan.lookfinity.Processes.task;

import com.ideaclan.lookfinity.Processes.richText.RichText;
import com.ideaclan.lookfinity.Processes.workflow.Workflow;
import javassist.NotFoundException;

import java.util.List;

public interface ITaskService {

    Task createTask(Task task ,Workflow workflow,RichText richText);
    Task updateTask(Long id,Task task) throws NotFoundException;
    Boolean deleteTask(Long id) ;

    List<Task> findAllTask();
    Task findTaskById(Long id);
    long countTask();
}
