package com.ideaclan.lookfinity.Processes.roles;

import com.ideaclan.lookfinity.Processes.roles.Roles;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RolesRepository extends JpaRepository<Roles,Long> {

}
