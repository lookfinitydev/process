package com.ideaclan.lookfinity.Processes.roles.resolver;

import com.ideaclan.lookfinity.Processes.roles.IRoleService;
import com.ideaclan.lookfinity.Processes.roles.Roles;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsQuery;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@DgsComponent
public class RolesQuery {

    @Autowired
    private IRoleService iRoleService;

    @DgsQuery( field = "findAllRoles")
    public List<Roles> findAllRoles() {
        return iRoleService.findAllRoles();
    }

    @DgsQuery( field = "findRolesById")
    public Roles findRolesById(Long id) throws Exception {
        return iRoleService.findRolesById(id);
    }
    @DgsQuery( field = "countRoles")
    public long countRoles() {
        return iRoleService.countRoles();
    }
}
