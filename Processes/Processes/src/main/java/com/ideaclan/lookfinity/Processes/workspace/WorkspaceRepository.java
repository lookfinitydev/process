package com.ideaclan.lookfinity.Processes.workspace;

import com.ideaclan.lookfinity.Processes.workspace.Workspaces;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WorkspaceRepository extends JpaRepository<Workspaces, Long> {
}
