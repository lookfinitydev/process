package com.ideaclan.lookfinity.Processes.task.resolver;

import com.ideaclan.lookfinity.Processes.task.ITaskService;
import com.ideaclan.lookfinity.Processes.task.Task;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsData;
import com.netflix.graphql.dgs.DgsQuery;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@DgsComponent
@Slf4j
public class TaskQuery {

    @Autowired
    private ITaskService iTaskService;

    @DgsQuery( field = "findAllTask")
    public List<Task> findAllTask() {
        return iTaskService.findAllTask();
    }

    @DgsQuery( field = "findTaskById")
    public Task findTaskById(Long id)  {
        return iTaskService.findTaskById(id);
    }

    @DgsQuery( field = "countTask")
    public long countTask() {
        return iTaskService.countTask();
    }
}
