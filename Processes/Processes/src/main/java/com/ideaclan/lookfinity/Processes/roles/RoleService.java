package com.ideaclan.lookfinity.Processes.roles;

import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class RoleService implements IRoleService{

    @Autowired
    private RolesRepository rolesRepository;

    @Override
    public Roles createRoles(Roles roles) {
        return rolesRepository.save(roles);
    }

    @Override
    public Roles updateRoles(Long id,Roles roles ) throws NotFoundException {
        Optional<Roles> optionalRoles = this.rolesRepository.findById(id);

        if(optionalRoles.isPresent())
        {
            Roles roles1 = optionalRoles.get();

            roles.setRoleName(roles.getRoleName());
            roles.setRoleDescription(roles.getRoleDescription());

            return rolesRepository.save(roles1);
        }
        throw new NotFoundException("Roles Details Not Found");
    }

    @Override
    public Boolean deleteRoles(Long id) {
        rolesRepository.deleteById(id);
        return true;
    }

    @Override
    public List<Roles> findAllRoles() {
        return rolesRepository.findAll();
    }

    @Override
    public Roles findRolesById(Long id) {
        return rolesRepository.findById(id).orElseThrow(null);
    }

    @Override
    public long countRoles() {
        return rolesRepository.count();
    }

}
