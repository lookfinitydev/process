package com.ideaclan.lookfinity.Processes.workflowRoles;

import com.ideaclan.lookfinity.Processes.workflowRoles.WorkflowRoles;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WorkflowRolesRepository extends JpaRepository<WorkflowRoles,Long> {
}
