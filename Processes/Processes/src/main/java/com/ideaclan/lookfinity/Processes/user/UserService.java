package com.ideaclan.lookfinity.Processes.user;

import com.ideaclan.lookfinity.Processes.roles.Roles;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class UserService implements IUserService{

    @Autowired
    private UserRepository userRepository;

    public User createUser(String userName, Roles roles)
    {
        User user = new User();
        user.setUserName(userName);
        user.setRoles(roles);
        userRepository.save(user);
        return user;
    }

    public User updateUser(Long userId,String userName, Roles roles) throws NotFoundException {
        Optional<User> optionalUser = this.userRepository.findById(userId);

        if(optionalUser.isPresent()) {
            User user = optionalUser.get();
            if(userName != null)
                user.setUserName(userName);
            if(roles != null)
                user.setRoles(roles);
            userRepository.save(user);
            return user;
        }
        throw new NotFoundException("User Details Nor Found");
    }

    public Boolean deleteUser(Long userId)  {
        userRepository.deleteById(userId);
        return true;
    }

    public List<User> findAllUser() {
        return userRepository.findAll();
    }

    public User findUserById(Long id){
        return userRepository.findById(id).orElseThrow();
    }

    public long countUser() {
        return userRepository.count();
    }
}
