package com.ideaclan.lookfinity.Processes.task.resolver;

import com.ideaclan.lookfinity.Processes.richText.RichText;
import com.ideaclan.lookfinity.Processes.task.ITaskService;
import com.ideaclan.lookfinity.Processes.task.Task;
import com.ideaclan.lookfinity.Processes.workflow.Workflow;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsMutation;
import com.netflix.graphql.dgs.InputArgument;
import javassist.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

@DgsComponent
@Slf4j
public class TaskMutation
{
      @Autowired
      private ITaskService iTaskService;

        @DgsMutation( field = "createTask")
        public Task createTask(@InputArgument("input") Task task ,Workflow workflow,RichText richText) {
           return iTaskService.createTask(task,workflow,richText);
        }

        @DgsMutation( field = "updateTask")
        public Task updateTask(Long id,Task task) throws NotFoundException {

            return iTaskService.updateTask(id,task);

        }

        @DgsMutation( field = "deleteTask")
        public Boolean deleteTask(Long id) {
            return  iTaskService.deleteTask(id);
        }

}
