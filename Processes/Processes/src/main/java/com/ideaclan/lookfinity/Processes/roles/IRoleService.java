package com.ideaclan.lookfinity.Processes.roles;

import javassist.NotFoundException;

import java.util.List;

public interface IRoleService {

    Roles createRoles(Roles roles);
    Boolean deleteRoles(Long id);
    Roles updateRoles(Long id,Roles roles) throws NotFoundException;

    List<Roles> findAllRoles();
    Roles findRolesById(Long id);
    long countRoles();
}
