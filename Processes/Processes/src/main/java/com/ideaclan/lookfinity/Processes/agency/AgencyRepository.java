package com.ideaclan.lookfinity.Processes.agency;

import com.ideaclan.lookfinity.Processes.agency.Agency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AgencyRepository extends JpaRepository<Agency, Long> {

}
