package com.ideaclan.lookfinity.Processes.workflow;

import com.ideaclan.lookfinity.Processes.workflow.Workflow;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WorkflowRepository extends JpaRepository<Workflow, Long>
{

}
