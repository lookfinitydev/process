package com.ideaclan.lookfinity.Processes.runs.resolver;

import com.ideaclan.lookfinity.Processes.runs.IRunsService;
import com.ideaclan.lookfinity.Processes.runs.Runs;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsQuery;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@DgsComponent
public class RunsQuery {

    @Autowired
    private IRunsService iRunsService;

    @DgsQuery( field = "findAllRuns")
    public List<Runs> findAllRuns() {
        return iRunsService.findAllRuns();
    }

    @DgsQuery( field = "findRunsById")
    public Runs findRunsById(Long id) throws Exception {
        return iRunsService.findRunsById(id);
    }
    @DgsQuery( field = "countRuns")
    public long countRuns() {
        return iRunsService.countRuns();
    }

}
