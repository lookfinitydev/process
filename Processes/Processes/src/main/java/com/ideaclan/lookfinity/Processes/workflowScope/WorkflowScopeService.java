package com.ideaclan.lookfinity.Processes.workflowScope;

import com.ideaclan.lookfinity.Processes.workflow.Workflow;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class WorkflowScopeService implements IWorkflowScope {

    @Autowired
    private WorkflowScopeRepository workflowScopeRepository;

    @Override
    public WorkflowScope createWorkflowScope(String scope , Workflow workflow) {
        WorkflowScope workflowScope = new WorkflowScope();
        workflowScope.setScope(scope);
        workflowScope.setWorkflow(workflow);
        return workflowScopeRepository.save(workflowScope);
    }

    @Override
    public WorkflowScope updateWorkflowScope(Long id,String scope ) throws NotFoundException {
        Optional<WorkflowScope> optionalWorkflowScope = this.workflowScopeRepository.findById(id);

        if(optionalWorkflowScope.isPresent())
        {
            WorkflowScope workflowScope = optionalWorkflowScope.get();
            workflowScope.setScope(scope);
            return workflowScopeRepository.save(workflowScope);
        }
        throw new NotFoundException("WorkflowScope Details Not Found");
    }

    @Override
    public Boolean deleteWorkflowScope(Long id) {
        workflowScopeRepository.deleteById(id);
        return true;
    }

    @Override
    public List<WorkflowScope> findAllWorkflowScopes() {
        return workflowScopeRepository.findAll();
    }

    @Override
    public WorkflowScope findWorkflowScopeById(Long id) {
        return workflowScopeRepository.findById(id).orElseThrow(null);
    }
}
