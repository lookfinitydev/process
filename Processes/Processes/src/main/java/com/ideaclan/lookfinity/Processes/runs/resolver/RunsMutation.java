package com.ideaclan.lookfinity.Processes.runs.resolver;

import com.ideaclan.lookfinity.Processes.runs.IRunsService;
import com.ideaclan.lookfinity.Processes.runs.Runs;
import com.ideaclan.lookfinity.Processes.status.Status;
import com.ideaclan.lookfinity.Processes.user.User;
import com.ideaclan.lookfinity.Processes.workflow.Workflow;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsMutation;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;

@DgsComponent
public class RunsMutation {

    @Autowired
    private IRunsService iRunsService;

    @DgsMutation(field = "createRuns")
    public Runs createRuns(Workflow workflow, User user, Status status)
    {
        return  iRunsService.createRuns(workflow, user, status);
    }

    @DgsMutation(field = "updateRuns")
    public Runs updateRuns(Long id,Workflow workflow, User user, Status status) throws NotFoundException {

        return iRunsService.updateRuns(id, workflow, user, status);
    }

    @DgsMutation( field = "deleteRuns")
    public Boolean deleteRuns(Long id) {
        return iRunsService.deleteRuns(id);
    }
}
