package com.ideaclan.lookfinity.Processes.workflowRoles.resolver;

import com.ideaclan.lookfinity.Processes.roles.Roles;
import com.ideaclan.lookfinity.Processes.workflow.Workflow;
import com.ideaclan.lookfinity.Processes.workflowRoles.IWorkflowRoles;
import com.ideaclan.lookfinity.Processes.workflowRoles.WorkflowRoles;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsMutation;
import javassist.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

@DgsComponent
@Slf4j
public class WorkflowRolesMutation {

    @Autowired
    private IWorkflowRoles iWorkflowRoles;

    @DgsMutation( field = "createWorkflowRoles")
    public WorkflowRoles createWorkflowRoles(Workflow workflow, Roles roles) {
        return iWorkflowRoles.createWorkflowRoles(workflow, roles);
    }

    @DgsMutation( field = "updateWorkflowRoles")
    public WorkflowRoles updateWorkflowRoles(Long  id,Workflow workflow, Roles roles) throws NotFoundException {
        return iWorkflowRoles.updateWorkflowRoles(id, workflow, roles);
    }

    @DgsMutation( field = "deleteWorkflowRoles")
    public Boolean deleteWorkflowRoles(Long id) {
        return iWorkflowRoles.deleteWorkflowRoles(id);
    }
}
