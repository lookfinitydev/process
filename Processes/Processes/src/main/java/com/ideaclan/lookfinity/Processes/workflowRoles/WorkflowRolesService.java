package com.ideaclan.lookfinity.Processes.workflowRoles;

import com.ideaclan.lookfinity.Processes.roles.Roles;
import com.ideaclan.lookfinity.Processes.workflow.Workflow;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class WorkflowRolesService implements IWorkflowRoles {

    @Autowired
    private WorkflowRolesRepository workflowRolesRepository;

    public WorkflowRoles createWorkflowRoles(Workflow workflow, Roles roles) {

        WorkflowRoles workflowRoles = new WorkflowRoles();
        workflowRoles.setWorkflow(workflow);
        workflowRoles.setRoles(roles);
        return workflowRolesRepository.save(workflowRoles);
    }

    public WorkflowRoles updateWorkflowRoles(Long  id,Workflow workflow, Roles roles) throws NotFoundException {
        Optional<WorkflowRoles> optionalWorkflowRoles = this.workflowRolesRepository.findById(id);

        if(optionalWorkflowRoles.isPresent())
        {
            WorkflowRoles workflowRoles = optionalWorkflowRoles.get();

            workflowRoles.setWorkflow(workflow);
            workflowRoles.setRoles(roles);
            return workflowRolesRepository.save(workflowRoles);
        }
        throw new NotFoundException("Workflow-Roles Details Not Found");
    }

    public Boolean deleteWorkflowRoles(Long id) {
        workflowRolesRepository.deleteById(id);
        return true;
    }

    public List<WorkflowRoles> findAllWorkflowRoles() {
        return workflowRolesRepository.findAll();
    }

    public WorkflowRoles findWorkflowRolesById(Long workflowRoleId) throws Exception {
        return workflowRolesRepository.findById(workflowRoleId).orElseThrow();
    }
}
