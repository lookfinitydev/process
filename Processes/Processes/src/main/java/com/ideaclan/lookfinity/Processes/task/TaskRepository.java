package com.ideaclan.lookfinity.Processes.task;

import com.ideaclan.lookfinity.Processes.task.Task;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskRepository extends JpaRepository<Task,Long> {
}
