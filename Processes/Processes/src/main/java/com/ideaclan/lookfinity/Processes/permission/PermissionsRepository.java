package com.ideaclan.lookfinity.Processes.permission;

import com.ideaclan.lookfinity.Processes.permission.Permissions;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PermissionsRepository extends JpaRepository<Permissions,Long> {
}
