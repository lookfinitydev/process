package com.ideaclan.lookfinity.Processes.runs;

import com.ideaclan.lookfinity.Processes.status.Status;
import com.ideaclan.lookfinity.Processes.user.User;
import com.ideaclan.lookfinity.Processes.workflow.Workflow;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class RunsService implements IRunsService{

    @Autowired
    private RunsRepository runsRepository;

    public Runs createRuns(Workflow workflow, User user, Status status)
    {
        Runs runs = new Runs();
        runs.setWorkflow(workflow);
        runs.setUser(user);
        runs.setStatus(status);
        runs.setCreateAt(new Date(System.currentTimeMillis()));
        return runsRepository.save(runs);
    }

    public Runs updateRuns(Long runsId,Workflow workflow, User user, Status status) throws NotFoundException {
        Optional<Runs> optionalRuns = this.runsRepository.findById(runsId);

        if(optionalRuns.isPresent()) {
            Runs runs = optionalRuns.get();

            runs.setWorkflow(workflow);
            runs.setUser(user);
            runs.setStatus(status);
            runs.setUpdateAt(new Date(System.currentTimeMillis()));
            return runsRepository.save(runs);
        }
        throw new NotFoundException("Runs Details Not Found");
    }


    public Boolean deleteRuns(Long id) {
        runsRepository.deleteById(id);
        return true;
    }

    @Override
    public List<Runs> findAllRuns() {
        return runsRepository.findAll();
    }

    @Override
    public Runs findRunsById(Long id) {
        return runsRepository.findById(id).orElseThrow(null);
    }

    @Override
    public long countRuns() {
        return runsRepository.count();
    }
}
