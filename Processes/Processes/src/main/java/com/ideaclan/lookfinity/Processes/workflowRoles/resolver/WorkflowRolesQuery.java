package com.ideaclan.lookfinity.Processes.workflowRoles.resolver;

import com.ideaclan.lookfinity.Processes.workflowRoles.IWorkflowRoles;
import com.ideaclan.lookfinity.Processes.workflowRoles.WorkflowRoles;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsQuery;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;

@DgsComponent
public class WorkflowRolesQuery {

    @Autowired
    private IWorkflowRoles iWorkflowRoles;

    @DgsQuery( field = "findAllWorkflowRoles")
    public List<WorkflowRoles> findAllWorkflowRoles() {
        return iWorkflowRoles.findAllWorkflowRoles();
    }

    @DgsQuery( field = "findWorkflowRolesById")
    public WorkflowRoles findWorkflowRolesById(Long id) throws Exception {
        return iWorkflowRoles.findWorkflowRolesById(id);
    }
}
