package com.ideaclan.lookfinity.Processes.runs;

import com.ideaclan.lookfinity.Processes.runs.Runs;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RunsRepository  extends JpaRepository<Runs,Long> {
}
