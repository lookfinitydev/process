package com.ideaclan.lookfinity.Processes.status;

import com.ideaclan.lookfinity.Processes.task.Task;
import com.ideaclan.lookfinity.Processes.user.User;
import javassist.NotFoundException;

import java.util.List;

public interface IStatusService {

    Status createStatus(Task task, User user, Boolean status);
    Status updateStatus(Long id, Task task, User user, Boolean status) throws NotFoundException;
    Boolean deleteStatus(Long id) ;

    List<Status> findAllStatus();
    Status findStatusById(Long id);
    long countStatus();
}
