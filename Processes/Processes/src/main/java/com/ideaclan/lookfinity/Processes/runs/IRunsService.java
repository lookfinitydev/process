package com.ideaclan.lookfinity.Processes.runs;

import com.ideaclan.lookfinity.Processes.status.Status;
import com.ideaclan.lookfinity.Processes.user.User;
import com.ideaclan.lookfinity.Processes.workflow.Workflow;
import javassist.NotFoundException;

import java.util.List;

public interface IRunsService {


    Runs createRuns(Workflow workflow, User user, Status status);
    Runs updateRuns(Long id,Workflow workflow, User user, Status status) throws NotFoundException;
    Boolean deleteRuns(Long id);

    List<Runs> findAllRuns();
    Runs findRunsById(Long id);
    long countRuns();
}
