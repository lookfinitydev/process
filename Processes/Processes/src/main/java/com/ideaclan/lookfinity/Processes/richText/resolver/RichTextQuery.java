package com.ideaclan.lookfinity.Processes.richText.resolver;

import com.ideaclan.lookfinity.Processes.richText.IRichTextService;
import com.ideaclan.lookfinity.Processes.richText.RichText;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.UnsupportedEncodingException;
import java.util.List;

@Slf4j
@DgsComponent
public class RichTextQuery {

    @Autowired
    private IRichTextService iRichTextService;

    @DgsData(parentType = "Query",field = "findAllRichText")
    public List<RichText> findAllRichText() {
        return iRichTextService.findAllRichText();
    }

    @DgsData(parentType = "Query",field = "findRichTextById")
    public RichText findRichTextById(Long id) throws Exception {
        return iRichTextService.findRichTextById(id);
    }
    @DgsData(parentType = "Query",field = "countRichText")
    public long countRichText() {
        return iRichTextService.countRichText();
    }

    @DgsData(parentType = "Query",field = "findByBody")
    public RichText findByBody(String body,Boolean isEncoded) throws UnsupportedEncodingException {

        return  iRichTextService.findByBody(body,isEncoded);
    }

}
