package com.ideaclan.lookfinity.Processes.user.resolver;

import com.ideaclan.lookfinity.Processes.user.IUserService;
import com.ideaclan.lookfinity.Processes.user.User;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsQuery;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;

@DgsComponent
public class UserQuery {

    @Autowired
    private IUserService iUserService;

    @DgsQuery( field = "findAllUser")
    public List<User> findAllUser() {
        return iUserService.findAllUser();
    }

    @DgsQuery( field = "findUserById")
    public User findUserById(Long id) throws Exception {
        return iUserService.findUserById(id);
    }
    @DgsQuery( field = "countUser")
    public long countUser() {
        return iUserService.countUser();
    }
}
