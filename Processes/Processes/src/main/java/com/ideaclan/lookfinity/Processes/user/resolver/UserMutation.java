package com.ideaclan.lookfinity.Processes.user.resolver;

import com.ideaclan.lookfinity.Processes.roles.Roles;
import com.ideaclan.lookfinity.Processes.user.IUserService;
import com.ideaclan.lookfinity.Processes.user.User;
import com.ideaclan.lookfinity.Processes.user.UserRepository;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsData;
import com.netflix.graphql.dgs.DgsMutation;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.Optional;

@DgsComponent
public class UserMutation {

    @Autowired
    private IUserService iUserService;

    @DgsMutation( field = "createUser")
    public User createUser(String userName, Roles roles)
    {
        return iUserService.createUser(userName, roles);
    }

    @DgsMutation( field = "updateUser")
    public User updateUser(Long id,String userName, Roles roles) throws NotFoundException {

        return iUserService.updateUser(id, userName, roles);
    }

    @DgsMutation( field = "deleteUser")
    public Boolean deleteUser(Long id)  {
       return iUserService.deleteUser(id);
    }

}
