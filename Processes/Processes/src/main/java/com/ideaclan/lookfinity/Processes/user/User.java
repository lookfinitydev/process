package com.ideaclan.lookfinity.Processes.user;

import com.ideaclan.lookfinity.Processes.roles.Roles;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
public class User
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_Name")
    private String userName;

    @ManyToOne
    @JoinColumn(name = "role_fk" )
    private Roles roles;

    @Nullable
    @Column(name = "created_on",nullable = false, updatable = false)
    @CreationTimestamp
    private LocalDateTime createdOn;

    @Nullable
    @Column(name = "updated_on")
    @UpdateTimestamp
    private LocalDateTime updatedOn;

    public User(Long id) {
        this.id = id;
    }
}
