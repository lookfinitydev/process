package com.ideaclan.lookfinity.Processes.richText;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
public class RichText {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Lob
    private String body;

    private Boolean flag;

    @Nullable
    @Column(name = "created_on",nullable = false, updatable = false)
    @CreationTimestamp
    private LocalDateTime createdOn;

    @Nullable
    @Column(name = "updated_on")
    @UpdateTimestamp
    private LocalDateTime updatedOn;

    public RichText(Long id) {
        this.id = id;
    }
}
