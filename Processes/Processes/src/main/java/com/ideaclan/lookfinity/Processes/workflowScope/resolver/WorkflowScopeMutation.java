package com.ideaclan.lookfinity.Processes.workflowScope.resolver;

import com.ideaclan.lookfinity.Processes.workflow.Workflow;
import com.ideaclan.lookfinity.Processes.workflowScope.IWorkflowScope;
import com.ideaclan.lookfinity.Processes.workflowScope.WorkflowScope;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsMutation;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;

@DgsComponent
public class WorkflowScopeMutation {

    @Autowired
    private IWorkflowScope iWorkflowScope;

    @DgsMutation( field = "createWorkflowScope")
    public WorkflowScope createWorkflowScope(String scope , Workflow workflow) {
       return iWorkflowScope.createWorkflowScope(scope, workflow);
    }
    @DgsMutation( field = "deleteWorkflowScope")
    public Boolean deleteWorkflowScope(Long id) {
        return  deleteWorkflowScope(id);
    }
    @DgsMutation( field = "updateWorkflowScope")
    public WorkflowScope updateWorkflowScope(Long id,String scope) throws NotFoundException {
       return  iWorkflowScope.updateWorkflowScope(id,scope);
    }
}
