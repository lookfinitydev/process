package com.ideaclan.lookfinity.Processes.workflow;

import com.ideaclan.lookfinity.Processes.runs.Runs;
import com.ideaclan.lookfinity.Processes.task.Task;
import com.ideaclan.lookfinity.Processes.workspace.Workspaces;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
public class Workflow
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "workflowName", nullable = false)
    private String workflowName;

    @Column(name = "description",length = 5000)
    private String description;

    @Column(name = "ownerId", nullable = false)
    private String ownerId;

    @Nullable
    @Column(name = "created_on",nullable = false, updatable = false)
    @CreationTimestamp
    private LocalDateTime createdOn;

    @Nullable
    @Column(name = "updated_on")
    @UpdateTimestamp
    private LocalDateTime updatedOn;

    @Column(name = "isRunnable")
    private Boolean isRunnable;

    @OneToMany(mappedBy = "workflow")
    private List<Task> task;

    @OneToMany(mappedBy = "workflow")
    private List<Runs> runs;

    @OneToMany
    @JoinTable(name = "Mapped",
            joinColumns = { @JoinColumn( name = "workflowId")},
            inverseJoinColumns = {@JoinColumn(name = "workspaceId")})
    private List<Workspaces> workspaces;

    public Workflow(Long id) {
        this.id = id;
    }
}
