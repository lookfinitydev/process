package com.ideaclan.lookfinity.Processes.workflowScope;

import org.springframework.data.jpa.repository.JpaRepository;

public interface WorkflowScopeRepository extends JpaRepository<WorkflowScope,Long> {
}
