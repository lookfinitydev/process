package com.ideaclan.lookfinity.Processes.roles;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@Setter
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Roles
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "roles_Name",nullable = false)
    private String roleName;

    @Column(name = "roles_Description",length = 5000)
    private String roleDescription;

    @Nullable
    @Column(name = "created_on",nullable = false, updatable = false)
    @CreationTimestamp
    private LocalDateTime createdOn;

    @Nullable
    @Column(name = "updated_on")
    @UpdateTimestamp
    private LocalDateTime updatedOn;

    public Roles(Long id) {
        this.id = id;
    }
}
