package com.ideaclan.lookfinity.Processes.user;

import com.ideaclan.lookfinity.Processes.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Long> {
}
