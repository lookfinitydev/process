package com.ideaclan.lookfinity.Processes.richText;

import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

@Service
public class RichTextService implements IRichTextService{

    @Autowired
    private RichTextRepository richTextRepository;

    public RichText createRichText(RichText richText) {

        return richTextRepository.save(richText);
    }

    public RichText updateRichText(Long id,RichText richText) throws NotFoundException {
        Optional<RichText> optionalRichText = this.richTextRepository.findById(id);

        if (optionalRichText.isPresent()) {
            RichText richText1 = optionalRichText.get();
            richText1.setBody(richText.getBody());
            return richTextRepository.save(richText1);
        }
        throw new NotFoundException("Rich Text Details Not Found");
    }

    public Boolean deleteRichText(Long id) {
        richTextRepository.deleteById(id);
        return true;
    }

    public List<RichText> findAllRichText() {
        return (List<RichText>) richTextRepository.findAll();
    }

    public RichText findRichTextById(Long id) throws Exception {
        return richTextRepository.findById(id).orElseThrow();
    }

    public long countRichText() {
        return richTextRepository.count();
    }

    public RichText findByBody(String body,Boolean isEncoded) throws UnsupportedEncodingException {
        RichText richText = richTextRepository.findByBody(body);
        String encodedString= richText.getBody();
        String decodedString = new String(Base64.getMimeDecoder().decode(encodedString.trim()));


        if(isEncoded == true)
        {
            richText.setBody(decodedString);
        }
        else
        {
            richText.setBody(encodedString);
        }
        return richText;
    }

}
