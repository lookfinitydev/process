package com.ideaclan.lookfinity.Processes.workflowScope.resolver;

import com.ideaclan.lookfinity.Processes.workflowScope.IWorkflowScope;
import com.ideaclan.lookfinity.Processes.workflowScope.WorkflowScope;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsQuery;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@DgsComponent
public class WorkflowScopeQuery {

    @Autowired
    private IWorkflowScope iWorkflowScope;

    @DgsQuery( field = "findAllWorkflowScopes")
    public List<WorkflowScope> findAllWorkflowScopes() {
        return iWorkflowScope.findAllWorkflowScopes();
    }

    @DgsQuery( field = "findWorkflowScopeById")
    public WorkflowScope findWorkflowScopeById(Long id) throws Exception {
        return iWorkflowScope.findWorkflowScopeById(id);
    }
}
