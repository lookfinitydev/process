package com.ideaclan.lookfinity.Processes.status;

import com.ideaclan.lookfinity.Processes.task.Task;
import com.ideaclan.lookfinity.Processes.user.User;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class StatusService implements IStatusService{

    @Autowired
    private StatusRepository statusRepository;

    public Status createStatus(Task task, User user, Boolean status)
    {
        Status status1 = new Status();
        status1.setTask(task);
        status1.setUser(user);
        status1.setStatus(status);
        return statusRepository.save(status1);
    }

    public Status updateStatus(Long id, Task task, User user, Boolean status) throws NotFoundException {
        Optional<Status> optionalStatus = this.statusRepository.findById(id);

        if (optionalStatus.isPresent()) {
            Status status1 = optionalStatus.get();
            status1.setTask(task);
            status1.setUser(user);
            status1.setStatus(status);
            return statusRepository.save(status1);
        }
        throw new NotFoundException("Status Details Not Found");
    }

    public Boolean deleteStatus(Long id) {
        statusRepository.deleteById(id);
        return true;
    }

    public List<Status> findAllStatus() {
        return statusRepository.findAll();
    }

    public Status findStatusById(Long id) {
        return statusRepository.findById(id).orElseThrow(null);
    }

    public long countStatus() {
        return statusRepository.count();
    }
}
