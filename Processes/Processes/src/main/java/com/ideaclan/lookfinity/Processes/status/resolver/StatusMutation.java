package com.ideaclan.lookfinity.Processes.status.resolver;

import com.ideaclan.lookfinity.Processes.status.Status;
import com.ideaclan.lookfinity.Processes.task.Task;
import com.ideaclan.lookfinity.Processes.user.User;
import com.ideaclan.lookfinity.Processes.status.StatusRepository;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsData;
import com.netflix.graphql.dgs.DgsMutation;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

@DgsComponent
public class StatusMutation {

    @Autowired
    private StatusRepository statusRepository;

    @DgsMutation( field = "createStatus")
    public Status createStatus(Task task, User user, Boolean status)
    {
        Status status1 = new Status();
        status1.setTask(task);
        status1.setUser(user);
        status1.setStatus(status);
        return statusRepository.save(status1);

    }
    @DgsMutation( field = "updateStatus")
    public Status updateStatus(Long id, Task task, User user, Boolean status) throws NotFoundException {
        Optional<Status> optionalStatus = this.statusRepository.findById(id);

        if (optionalStatus.isPresent()) {
            Status status1 = optionalStatus.get();
            status1.setTask(task);
            status1.setUser(user);

            status1.setStatus(status);
            statusRepository.save(status1);
            return status1;
        }
        throw new NotFoundException("Workflow Details Nor Found");
    }

    @DgsMutation( field = "deleteStatus")
    public Boolean deleteStatus(Long id) {
        statusRepository.deleteById(id);
        return true;
    }

}
