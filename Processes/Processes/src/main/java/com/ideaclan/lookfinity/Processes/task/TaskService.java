package com.ideaclan.lookfinity.Processes.task;

import com.ideaclan.lookfinity.Processes.richText.RichText;
import com.ideaclan.lookfinity.Processes.workflow.Workflow;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;


@Service
public class TaskService implements  ITaskService{

    @Autowired
    private TaskRepository taskRepository;

    public Task createTask(Task task, Workflow workflow, RichText richText) {

        task.setWorkflow(workflow);
        task.setRichText(richText);
        return taskRepository.save(task);

    }

    public Boolean deleteTask(Long id) {
        taskRepository.deleteById(id);
        return true;
    }

    public Task updateTask(Long id, Task task) throws NotFoundException {

        Optional<Task> optionalTask = this.taskRepository.findById(id);

        if(optionalTask.isPresent())
        {
            Task task1 = optionalTask.get();
            task.setTaskName(task.getTaskName());

            return taskRepository.save(task);
        }
        throw new NotFoundException("Task Details Not Found");
    }

    public List<Task> findAllTask() {
        return taskRepository.findAll();
    }

    public Task findTaskById(Long id)  {
        return taskRepository.findById(id).orElseThrow();
    }

    public long countTask() {
        return taskRepository.count();
    }

}
