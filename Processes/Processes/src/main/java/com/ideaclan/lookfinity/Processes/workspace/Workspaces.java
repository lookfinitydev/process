package com.ideaclan.lookfinity.Processes.workspace;

import com.ideaclan.lookfinity.Processes.agency.Agency;
import lombok.*;

import javax.persistence.*;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
public class Workspaces {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Agency agency;

}
