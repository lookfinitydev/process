package com.ideaclan.lookfinity.Processes.workflow;

import com.ideaclan.lookfinity.Processes.workspace.Workspaces;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class WorkflowService implements IWorkflowService {

    @Autowired
    private WorkflowRepository workflowRepository;

    @Override
    public Workflow createWorkflow(Workflow workflow) {

        return workflowRepository.save(workflow);

    }

    @Override
    public Workflow updateWorkflow(Long id,Workflow workflow) throws NotFoundException {

        Optional<Workflow> optionalWorkflow = this.workflowRepository.findById(id);

        if(optionalWorkflow.isPresent())
        {
            Workflow workflow1 = optionalWorkflow.get();

            workflow1.setWorkflowName(workflow.getWorkflowName());
            workflow1.setDescription(workflow.getDescription());
            workflow1.setOwnerId(workflow.getOwnerId());
            workflow1.setIsRunnable(workflow.getIsRunnable());

            return workflowRepository.save(workflow1);
        }
        throw new NotFoundException("Workflow Details Not Found");
    }

    @Override
    public Boolean deleteWorkflow(Long id) {
        workflowRepository.deleteById(id);
        return true;
    }

    @Override
    public Workflow addWorkspacesToWorkflow(Long workflowId, Workspaces workspaces) {
        Optional<Workflow> optionalWorkflow =this.workflowRepository.findById(workflowId);
        Workflow workflow =optionalWorkflow.get();
        List<Workspaces> workspacesList = new ArrayList<>();
        workspacesList.add(workspaces);
        workflow.setWorkspaces(workspacesList);
        return workflow;
    }

    @Override
    public List<Workflow> findAllWorkflow() {
        return workflowRepository.findAll();
    }

    @Override
    public Workflow findWorkflowById(Long id) {
        return workflowRepository.findById(id).orElseThrow(null);
    }

    @Override
    public long countWorkflows() {
        return workflowRepository.count();
    }
}