package com.ideaclan.lookfinity.Processes.workflowScope;

import com.ideaclan.lookfinity.Processes.workflow.Workflow;
import javassist.NotFoundException;

import java.util.List;

public interface IWorkflowScope {

    WorkflowScope createWorkflowScope(String scope , Workflow workflow);
    WorkflowScope updateWorkflowScope(Long id,String scope ) throws NotFoundException;
    Boolean deleteWorkflowScope(Long id);

    List<WorkflowScope> findAllWorkflowScopes();
    WorkflowScope findWorkflowScopeById(Long id);
}
