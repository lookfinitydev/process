package com.ideaclan.lookfinity.Processes.rolesPermissions.resolver;

import com.ideaclan.lookfinity.Processes.rolesPermissions.RolesPermissions;
import com.ideaclan.lookfinity.Processes.rolesPermissions.RolesPermissionsReposittory;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsData;
import org.springframework.beans.factory.annotation.Autowired;

@DgsComponent
public class RolesPermissionsQuery {

    @Autowired
    private RolesPermissionsReposittory rolesPermissionsReposittory;

    @DgsData(parentType = "Query",field = "findAllRolesPermissions")
    public Iterable<RolesPermissions> findAllRolesPermissions() {
        return rolesPermissionsReposittory.findAll();
    }

    @DgsData(parentType = "Query",field = "findRolesPermissionsById")
    public RolesPermissions findRolesPermissionsById(Long rolesPermissionsId) throws Exception {
        return rolesPermissionsReposittory.findById(rolesPermissionsId).orElseThrow();
    }
    @DgsData(parentType = "Query",field = "countRolesPermissions")
    public long countRolesPermissions() {
        return rolesPermissionsReposittory.count();
    }
}
