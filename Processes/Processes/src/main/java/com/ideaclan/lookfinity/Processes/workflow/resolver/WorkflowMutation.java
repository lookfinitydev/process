package com.ideaclan.lookfinity.Processes.workflow.resolver;

import com.ideaclan.lookfinity.Processes.workflow.IWorkflowService;
import com.ideaclan.lookfinity.Processes.workflow.Workflow;
import com.ideaclan.lookfinity.Processes.workspace.Workspaces;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsMutation;
import com.netflix.graphql.dgs.InputArgument;
import javassist.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

@DgsComponent
@Slf4j
public class WorkflowMutation
{
    @Autowired
    private IWorkflowService iWorkflowService;

    @DgsMutation(field = "createWorkflow")
    public Workflow createWorkflow(@InputArgument("input")Workflow workflow)  {

        return iWorkflowService.createWorkflow(workflow);

    }

    @DgsMutation(field = "deleteWorkflow")
    public Boolean deleteWorkflow(Long id) {

        return iWorkflowService.deleteWorkflow(id);
    }

    @DgsMutation(field = "updateWorkflow")
    public Workflow updateWorkflow(Long id,Workflow workflow) throws NotFoundException {

        return iWorkflowService.updateWorkflow(id, workflow);
    }

    @DgsMutation(field = "addWorkspacesToWorkflow")
    public Workflow addWorkspacesToWorkflow(Long workflowId,Workspaces workspaces) {
       return iWorkflowService.addWorkspacesToWorkflow(workflowId,workspaces);
    }

}
