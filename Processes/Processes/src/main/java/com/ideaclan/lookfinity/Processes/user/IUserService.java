package com.ideaclan.lookfinity.Processes.user;

import com.ideaclan.lookfinity.Processes.roles.Roles;
import javassist.NotFoundException;

import java.util.List;

public interface IUserService {

    User createUser(String userName, Roles roles);
    User updateUser(Long userId,String userName, Roles roles) throws NotFoundException;
    Boolean deleteUser(Long userId);

    List<User> findAllUser();
    User findUserById(Long id);
    long countUser();
}
