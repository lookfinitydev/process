package com.ideaclan.lookfinity.Processes.agency;

import lombok.*;

import javax.persistence.*;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
public class Agency
{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


}
