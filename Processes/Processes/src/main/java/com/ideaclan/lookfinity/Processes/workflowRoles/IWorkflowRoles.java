package com.ideaclan.lookfinity.Processes.workflowRoles;

import com.ideaclan.lookfinity.Processes.roles.Roles;
import com.ideaclan.lookfinity.Processes.workflow.Workflow;
import javassist.NotFoundException;

import java.util.List;

public interface IWorkflowRoles {

    WorkflowRoles createWorkflowRoles(Workflow workflow, Roles roles);
    WorkflowRoles updateWorkflowRoles(Long  id,Workflow workflow, Roles roles) throws NotFoundException;
    Boolean deleteWorkflowRoles(Long id);

    List<WorkflowRoles> findAllWorkflowRoles();
    WorkflowRoles findWorkflowRolesById(Long id) throws Exception;
}
