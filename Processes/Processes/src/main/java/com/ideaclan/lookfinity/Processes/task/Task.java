package com.ideaclan.lookfinity.Processes.task;

import com.ideaclan.lookfinity.Processes.richText.RichText;
import com.ideaclan.lookfinity.Processes.workflow.Workflow;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Setter
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Task
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "taskTitle")
    private String taskName;

    @Nullable
    @Column(name = "created_on",nullable = false, updatable = false)
    @CreationTimestamp
    private LocalDateTime createdOn;

    @Nullable
    @Column(name = "updated_on")
    @UpdateTimestamp
    private LocalDateTime updatedOn;


    @ManyToOne
    @JoinColumn(name = "workFlowId")
    private Workflow workflow;

    @OneToOne
    @JoinColumn(name = "richTextId")
    private RichText richText;

    public Task(Long id) {
        this.id = id;
    }
}
