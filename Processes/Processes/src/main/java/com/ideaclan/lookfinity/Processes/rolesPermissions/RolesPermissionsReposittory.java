package com.ideaclan.lookfinity.Processes.rolesPermissions;

import com.ideaclan.lookfinity.Processes.rolesPermissions.RolesPermissions;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RolesPermissionsReposittory extends JpaRepository<RolesPermissions,Long> {
}
