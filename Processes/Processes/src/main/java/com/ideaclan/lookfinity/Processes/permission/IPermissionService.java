package com.ideaclan.lookfinity.Processes.permission;

import com.netflix.graphql.dgs.InputArgument;
import javassist.NotFoundException;
import java.util.List;

public interface IPermissionService {


    Permissions createPermission(@InputArgument("input")Permissions permissions);
    Permissions updatePermission(Long permissionId,Permissions permissions) throws NotFoundException;
    Boolean deletePermission(Long id);

    List<Permissions> findAllPermission();
    Permissions findPermissionById(Long id);
}
