package com.ideaclan.lookfinity.Processes.rolesPermissions;

import com.ideaclan.lookfinity.Processes.permission.Permissions;
import com.ideaclan.lookfinity.Processes.roles.Roles;
import javassist.NotFoundException;

public interface IRolesPermissions {

    RolesPermissions createRolesPermissions(String description, Permissions permissions, Roles roles);
    Boolean deleteRolesPermissions(Long id);
    RolesPermissions updateRolesPermissions(Long rolesPermissionId) throws NotFoundException;

}
