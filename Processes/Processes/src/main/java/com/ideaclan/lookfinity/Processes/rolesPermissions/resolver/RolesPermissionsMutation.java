package com.ideaclan.lookfinity.Processes.rolesPermissions.resolver;

import com.ideaclan.lookfinity.Processes.permission.Permissions;
import com.ideaclan.lookfinity.Processes.roles.Roles;
import com.ideaclan.lookfinity.Processes.rolesPermissions.IRolesPermissions;
import com.ideaclan.lookfinity.Processes.rolesPermissions.RolesPermissions;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsData;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;


@DgsComponent
public class RolesPermissionsMutation {

    @Autowired
    private IRolesPermissions iRolesPermissions;

    @DgsData(parentType = "Mutation", field = "createRolesPermissions")
    public RolesPermissions createRolesPermissions(String description,Permissions permissions,Roles roles) {
       return  iRolesPermissions.createRolesPermissions(description, permissions, roles);
    }
    @DgsData(parentType = "Mutation", field = "deleteRolesPermissions")
    public Boolean deleteRolesPermissions(Long id) {
        return iRolesPermissions.deleteRolesPermissions(id);
    }
    @DgsData(parentType = "Mutation", field = "updateRolesPermissions")
    public RolesPermissions updateRolesPermissions(Long id) throws NotFoundException {

        return iRolesPermissions.updateRolesPermissions(id);
    }
}
