package com.ideaclan.lookfinity.Processes.status.resolver;

import com.ideaclan.lookfinity.Processes.status.Status;
import com.ideaclan.lookfinity.Processes.status.StatusRepository;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsQuery;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@DgsComponent
public class StatusQuery {

    @Autowired
    private StatusRepository statusRepository;

    @DgsQuery( field = "findAllStatus")
    public List<Status> findAllStatus() {
        return statusRepository.findAll();
    }

    @DgsQuery( field = "findStatusById")
    public Status findStatusById(Long id) throws Exception {
        return statusRepository.findById(id).orElseThrow();
    }
    @DgsQuery( field = "countStatus")
    public long countStatus() {
        return statusRepository.count();
    }
}
