package com.ideaclan.lookfinity.Processes.permission;

import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class PermissionService implements  IPermissionService{

    @Autowired
    private PermissionsRepository permissionsRepository;

    @Override
    public Permissions createPermission(Permissions permissions) {
        return permissionsRepository.save(permissions);
    }

    @Override
    public Permissions updatePermission(Long id, Permissions permissions) throws NotFoundException {
        Optional<Permissions> optionalPermissions = this.permissionsRepository.findById(id);

        if(optionalPermissions.isPresent())
        {
            Permissions permissions1 = optionalPermissions.get();

            permissions.setPermissionName(permissions.getPermissionName());
            permissions.setPermissionDescription(permissions.getPermissionDescription());

            return permissionsRepository.save(permissions);
        }
        throw new NotFoundException("Permissions Details Not Found");
    }

    @Override
    public Boolean deletePermission(Long id) {
         permissionsRepository.deleteById(id);
         return  true;
    }

    @Override
    public List<Permissions> findAllPermission() {
        return permissionsRepository.findAll();
    }

    @Override
    public Permissions findPermissionById(Long id) {
        return permissionsRepository.findById(id).orElseThrow(null);
    }
}
