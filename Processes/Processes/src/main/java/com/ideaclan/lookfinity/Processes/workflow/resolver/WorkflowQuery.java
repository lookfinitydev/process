package com.ideaclan.lookfinity.Processes.workflow.resolver;

import com.ideaclan.lookfinity.Processes.workflow.IWorkflowService;
import com.ideaclan.lookfinity.Processes.workflow.Workflow;
import com.ideaclan.lookfinity.Processes.workflow.WorkflowRepository;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsData;
import com.netflix.graphql.dgs.DgsQuery;
import org.springframework.beans.factory.annotation.Autowired;

@DgsComponent
public class WorkflowQuery {

    @Autowired
    private IWorkflowService iWorkflowService;

    @DgsQuery( field = "findAllWorkflow")
    public Iterable<Workflow> findAllWorkflow() {
        return iWorkflowService.findAllWorkflow();
    }

    @DgsQuery( field = "findWorkflowById")
    public Workflow findWorkflowById(Long id) throws Exception {
        return iWorkflowService.findWorkflowById(id);
    }

    @DgsQuery( field = "countWorkflows")
    public long countWorkflows() {
        return iWorkflowService.countWorkflows();
    }
}
