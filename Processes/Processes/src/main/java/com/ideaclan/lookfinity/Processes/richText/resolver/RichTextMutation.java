package com.ideaclan.lookfinity.Processes.richText.resolver;

import com.ideaclan.lookfinity.Processes.richText.IRichTextService;
import com.ideaclan.lookfinity.Processes.richText.RichText;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsMutation;
import com.netflix.graphql.dgs.InputArgument;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;

@DgsComponent
public class RichTextMutation {

    @Autowired
    private IRichTextService iRichTextService;

    @DgsMutation( field = "createRichText")
    public RichText createRichText(@InputArgument("input") RichText richText) {

        return iRichTextService.createRichText(richText);
    }

    @DgsMutation( field = "updateRichText")
    public RichText updateRichText(Long id,RichText richText) throws NotFoundException {
      return  iRichTextService.updateRichText(id, richText);
    }

    @DgsMutation( field = "deleteRichText")
    public Boolean deleteRichText(Long id) {
        return iRichTextService.deleteRichText(id);
    }
}
