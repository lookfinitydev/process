package com.ideaclan.lookfinity.Processes.status;

import com.ideaclan.lookfinity.Processes.user.User;
import com.ideaclan.lookfinity.Processes.task.Task;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity

public class Status
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "task_ID")
    private Task task;

    @OneToOne
    @JoinColumn(name = "user_ID")
    private User user;

    @Column(name = "Status")
    private Boolean status;

    @Nullable
    @Column(name = "created_on",nullable = false, updatable = false)
    @CreationTimestamp
    private LocalDateTime createdOn;

    @Nullable
    @Column(name = "updated_on")
    @UpdateTimestamp
    private LocalDateTime updatedOn;

    public Status(Long id) {
        this.id = id;
    }
}
