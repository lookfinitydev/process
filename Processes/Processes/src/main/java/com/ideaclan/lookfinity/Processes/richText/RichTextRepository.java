package com.ideaclan.lookfinity.Processes.richText;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RichTextRepository extends CrudRepository<RichText,Long> {

    public RichText findByBody(String body);
}
