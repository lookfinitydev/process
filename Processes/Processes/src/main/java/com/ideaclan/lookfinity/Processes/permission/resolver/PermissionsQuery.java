package com.ideaclan.lookfinity.Processes.permission.resolver;

import com.ideaclan.lookfinity.Processes.permission.IPermissionService;
import com.ideaclan.lookfinity.Processes.permission.Permissions;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsQuery;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;

@DgsComponent
public class PermissionsQuery {

    @Autowired
    private IPermissionService iPermissionService;

    @DgsQuery( field = "findAllPermission")
    public List<Permissions> findAllPermission() {
        return iPermissionService.findAllPermission();
    }

    @DgsQuery( field = "findPermissionById")
    public Permissions findPermissionById(Long id) throws Exception {
        return iPermissionService.findPermissionById(id);
    }
}
