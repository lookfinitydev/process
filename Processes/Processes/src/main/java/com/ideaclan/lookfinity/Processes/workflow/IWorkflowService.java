package com.ideaclan.lookfinity.Processes.workflow;

import com.ideaclan.lookfinity.Processes.workspace.Workspaces;
import javassist.NotFoundException;

public interface IWorkflowService
{
    Workflow createWorkflow(Workflow workflow);
    Workflow updateWorkflow(Long id,Workflow workflow) throws NotFoundException;
    Boolean deleteWorkflow(Long id);
    Workflow addWorkspacesToWorkflow(Long workflowId, Workspaces workspaces);

    Iterable<Workflow> findAllWorkflow();
    Workflow findWorkflowById(Long workflowId);
    long countWorkflows();
}
