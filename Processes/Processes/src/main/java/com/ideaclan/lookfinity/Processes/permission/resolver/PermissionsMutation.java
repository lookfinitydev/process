package com.ideaclan.lookfinity.Processes.permission.resolver;

import com.ideaclan.lookfinity.Processes.permission.IPermissionService;
import com.ideaclan.lookfinity.Processes.permission.Permissions;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsMutation;
import com.netflix.graphql.dgs.InputArgument;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;

@DgsComponent
public class PermissionsMutation {

    @Autowired
    private IPermissionService iPermissionService;

    @DgsMutation( field = "createPermission")
    public Permissions createPermission(@InputArgument("input")Permissions permissions) {

       return  iPermissionService.createPermission(permissions);
    }

    @DgsMutation( field = "updatePermission")
    public Permissions updatePermission(Long id,Permissions permissions) throws NotFoundException {

        return iPermissionService.updatePermission(id,permissions);

    }

    @DgsMutation( field = "deletePermission")
    public Boolean deletePermission(Long id) {

        return  iPermissionService.deletePermission(id);
    }

}
