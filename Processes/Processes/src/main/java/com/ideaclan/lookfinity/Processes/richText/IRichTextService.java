package com.ideaclan.lookfinity.Processes.richText;

import javassist.NotFoundException;

import java.io.UnsupportedEncodingException;
import java.util.List;

public interface IRichTextService {

    RichText createRichText(RichText richText) ;
    RichText updateRichText(Long id,RichText richText) throws NotFoundException ;
    Boolean deleteRichText(Long id);

    List<RichText> findAllRichText();
    RichText findRichTextById(Long id) throws Exception;
    long countRichText();
    RichText findByBody(String body,Boolean isEncoded) throws UnsupportedEncodingException;
}
