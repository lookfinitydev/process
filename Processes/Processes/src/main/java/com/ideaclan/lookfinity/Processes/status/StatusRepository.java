package com.ideaclan.lookfinity.Processes.status;

import com.ideaclan.lookfinity.Processes.status.Status;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatusRepository extends JpaRepository<Status,Long> {
}
