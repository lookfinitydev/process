package com.ideaclan.lookfinity.Processes.roles.resolver;

import com.ideaclan.lookfinity.Processes.roles.IRoleService;
import com.ideaclan.lookfinity.Processes.roles.Roles;
import com.ideaclan.lookfinity.Processes.roles.RolesRepository;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsData;
import com.netflix.graphql.dgs.DgsMutation;
import com.netflix.graphql.dgs.InputArgument;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.Optional;

@DgsComponent
public class RolesMutation {

    @Autowired
    private IRoleService iRoleService;

    @DgsMutation( field = "createRoles")
    public Roles createRoles(@InputArgument("input") Roles roles) {
        return iRoleService.createRoles(roles);
    }

    @DgsMutation( field = "deleteRoles")
    public Boolean deleteRoles(Long id) {
        return iRoleService.deleteRoles(id);
    }

    @DgsMutation( field = "updateRoles")
    public Roles updateRoles(Long id, Roles roles) throws NotFoundException {

        return iRoleService.updateRoles(id, roles);
    }

}
