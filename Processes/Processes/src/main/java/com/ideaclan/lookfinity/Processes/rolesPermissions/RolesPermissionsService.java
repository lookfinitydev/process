package com.ideaclan.lookfinity.Processes.rolesPermissions;

import com.ideaclan.lookfinity.Processes.permission.Permissions;
import com.ideaclan.lookfinity.Processes.roles.Roles;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RolesPermissionsService implements IRolesPermissions {

    @Autowired
    private RolesPermissionsReposittory rolesPermissionsReposittory;

    public RolesPermissions createRolesPermissions(String description, Permissions permissions, Roles roles) {
        RolesPermissions rolesPermissions = new RolesPermissions();
        rolesPermissions.setDescription(description);
        rolesPermissions.setPermissions(permissions);
        rolesPermissions.setRoles(roles);
        rolesPermissionsReposittory.save(rolesPermissions);
        return  rolesPermissions;
    }

    public Boolean deleteRolesPermissions(Long id) {
        rolesPermissionsReposittory.deleteById(id);
        return true;
    }

    public RolesPermissions updateRolesPermissions(Long id) throws NotFoundException {
        Optional<RolesPermissions> optionalRolesPermissions = this.rolesPermissionsReposittory.findById(id);

        if(optionalRolesPermissions.isPresent())
        {
            RolesPermissions rolesPermissions = optionalRolesPermissions.get();
            rolesPermissionsReposittory.save(rolesPermissions);
            return rolesPermissions;
        }
        throw new NotFoundException("Roles-Permissions Details Not Found");
    }

}
